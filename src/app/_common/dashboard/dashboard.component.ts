import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models/user';
import { EmployeeService } from 'src/app/_services/employee.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user: User;

  constructor(private empService: EmployeeService) {
    this.user = this.empService.userValue.data;
    console.log(this.user);
  }

  ngOnInit(): void {
  }

}
