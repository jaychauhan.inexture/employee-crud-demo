export class Employee {
    id: number;
    firstName: string;
    lastname: string;
    email: string;
    age: number;
    salary: number;
    active: boolean;
}