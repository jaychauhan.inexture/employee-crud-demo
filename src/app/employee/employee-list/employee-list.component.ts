import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Employee } from 'src/app/employee';
import { CommonService } from 'src/app/_services/common.service';
import { EmployeeService } from 'src/app/_services/employee.service';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: Observable<Employee[]>;
  displayModeEnum = DisplayModeEnum;
  displayMode: DisplayModeEnum;
  dtOptions: DataTables.Settings = {};
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();
  search = "";

  page = 1;
  count = 0;
  tableSize = 3;
  tableSizes = [3, 6, 9, 12];

  constructor(private employeeService: EmployeeService, private router: Router, private commonService: CommonService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };

    this.displayMode = DisplayModeEnum.UserList;
    this.reloadData();
  }

  changeDisplayMode(mode: DisplayModeEnum) {
    this.displayMode = mode;
  }


  /* fetch the data - reload employee list */
  reloadData() {
    this.employeeService.getEmployeesList().subscribe(data => {
      this.employees = data.data;
      // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    });
    //this.employees = this.employeeService.getEmployeesList();
  }

  /* delete the employee by calling employee service */
  deleteEmployee(id: number) {
    if (confirm("Are you sure to delete")) {
      this.employeeService.deleteEmployee(id).subscribe(data => {
        this.commonService.success('Employee deleted successfully', 'Employee');
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        this.reloadData();
      },
        error => {
          this.commonService.error(error, 'Error');
        });
    }

  }

  onTableDataChange(event) {
    this.page = event;
    //this.reloadData();
  }

  onTableSizeChange(event): void {
    this.tableSize = event.target.value;
    this.page = 1;
    //this.reloadData();
  }

}

enum DisplayModeEnum {
  UserList = 0,
  DatatableList = 1,
}
