import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { CommonService } from 'src/app/_services/common.service';
import { Employee } from '../../employee';
import { EmployeeService } from '../../_services/employee.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  employee: Employee = new Employee();
  submitted = false;
  imgFile: string;

  empForm = this.fb.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    age: [''],
    salary: [''],
    image: ['', Validators.required],
    imageSource: ['', Validators.required],
  });

  constructor(private employeeService: EmployeeService, private router: Router, private fb: FormBuilder, private commonService: CommonService) { }

  ngOnInit(): void {
  }

  /* create new employee */
  newEmployee() {
    this.submitted = false;
    this.empForm.reset();
  }

  onImageChange(e) {
    const reader = new FileReader();

    if (e.target.files && e.target.files.length) {
      const [file] = e.target.files;
      reader.readAsDataURL(file);
      const file1 = (e.target as HTMLInputElement).files[0];


      reader.onload = () => {
        this.imgFile = reader.result as string;
        this.empForm.patchValue({
          //imageSource: reader.result
          imageSource: file1
        });
      };
    }
  }

  /* call save employee service for save the employee */
  save() {
    this.employeeService.createEmployee(this.empForm.value).subscribe(data => {
      this.commonService.success('Employee added successfully', 'Employee');
      this.empForm.reset();
      this.gotoList();
    },
      error => {
        this.commonService.error(error, 'Error');
      });
  }

  onSubmit() {
    if (this.empForm.valid) {
      this.submitted = true;
      this.save();
    }
  }

  /* navigate to employee route */
  gotoList() {
    this.router.navigate(['/employees']);
  }

  get empFormControl() {
    return this.empForm.controls;
  }
}
