import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from 'src/app/employee';
import { CommonService } from 'src/app/_services/common.service';
import { EmployeeService } from 'src/app/_services/employee.service';


@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {
  id: number;
  employee: Employee;
  submitted = false;

  empForm = this.fb.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    age: [''],
    salary: [''],
  });

  constructor(private employeeService: EmployeeService, private router: Router, private route: ActivatedRoute, private fb: FormBuilder, private commonService: CommonService) { }

  ngOnInit(): void {
    this.employee = new Employee();
    this.id = this.route.snapshot.params['id'];

    /* fetch employee by id */
    this.employeeService.getEmployee(this.id).subscribe(data => {
      this.empForm.patchValue(data.data);
      console.log(this.empForm);
    }, error => console.log(error));
  }

  /* update the employee */
  updateEmployee() {
    this.employeeService.updateEmployee(this.id, this.empForm.value).subscribe(data => {
      this.commonService.success('Employee updated successfully', 'Employee');
      this.submitted = true;
      this.employee = new Employee();
      this.gotoList();
    }, error => {
      this.commonService.error(error, 'Error');
    });
  }

  onSubmit() {
    this.updateEmployee();
  }

  /* navigate to employee route */
  gotoList() {
    this.router.navigate(['/employees']);
  }

  get empFormControl() {
    return this.empForm.controls;
  }

}
