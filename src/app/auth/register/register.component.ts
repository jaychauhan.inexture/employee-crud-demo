import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CommonService } from 'src/app/_services/common.service';
import { EmployeeService } from 'src/app/_services/employee.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  submitted = false;
  loading = false;

  registerForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
    password_confirmation: ['', [Validators.required]]
  });

  constructor(private employeeService: EmployeeService, private router: Router, private fb: FormBuilder, private route: ActivatedRoute, private commonService: CommonService) { }

  ngOnInit(): void {
  }

  // convenience getter for easy access to form fields
  get form() { return this.registerForm.controls; }

  /* submit register form */
  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    // this.alertService.clear();

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.employeeService.register(this.registerForm.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.commonService.success('Register successfully...', 'Register');
          //this.alertService.success('Registration successful', { keepAfterRouteChange: true });
          this.router.navigate(['../login'], { relativeTo: this.route });
        },
        error: error => {
          this.commonService.error(error, 'Error');
          //this.alertService.error(error);
          this.loading = false;
        }
      });
  }
}