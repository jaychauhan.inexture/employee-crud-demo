import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { first } from 'rxjs/operators';
import { CommonService } from 'src/app/_services/common.service';
import { EmployeeService } from 'src/app/_services/employee.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submitted = false;
  loading = false;
  socialUser: SocialUser;

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
  });

  constructor(private employeeService: EmployeeService, private router: Router, private fb: FormBuilder, private route: ActivatedRoute, private commonService: CommonService, private socialAuthService: SocialAuthService) { }

  ngOnInit(): void {
  }

  /* submit login form */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.employeeService.login(this.form.email.value, this.form.password.value)
      .pipe(first())
      .subscribe({
        next: () => {
          // get return url from query parameters or default to home page
          this.commonService.success('you are logged in successfully...', 'Login');
          const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
          this.router.navigateByUrl(returnUrl);
        },
        error: error => {
          console.log(error);
          this.commonService.error(error, 'Error');
          this.loading = false;
        }
      });
  }

  /* login with google social account */
  loginWithGoogle(): void {
    this.submitted = true;
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(response => {
      this.employeeService.socialLogin('google', response)
        .pipe(first())
        .subscribe({
          next: () => {
            // get return url from query parameters or default to home page
            this.commonService.success('you are logged in successfully...', 'Login');
            const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
            this.router.navigateByUrl(returnUrl);
          },
          error: error => {
            console.log(error);
            this.commonService.error(error, 'Error');
            this.loading = false;
          }
        });
    });
  }

  logOut(): void {
    this.socialAuthService.signOut();
  }

  // convenience getter for easy access to form fields
  get form() { return this.loginForm.controls; }

}
