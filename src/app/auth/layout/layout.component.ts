import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/_services/employee.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(private router: Router, private empService: EmployeeService) {
    if (this.empService.userValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
  }

}
