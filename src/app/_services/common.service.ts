import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private toastr: ToastrService) { }

  /* get the item by key */
  getItem(key) {
    return localStorage.getItem(key);
  }

  /* store key value on local storage */
  setItem(key, item) {
    localStorage.setItem(key, JSON.stringify(item));
  }

  /* remove key from local storage */
  removeItem(key) {
    localStorage.removeItem(key);
  }

  success(message, title) {
    this.toastr.success(message, title);
  }

  error(message, title) {
    this.toastr.error(message, title);
  }

  warning(message, title) {
    this.toastr.warning(message, title);
  }

  info(message, title) {
    this.toastr.info(message, title);
  }
}
