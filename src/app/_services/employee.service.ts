import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { User } from '../_models/user';
import { map } from 'rxjs/operators';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private userSubject: BehaviorSubject<any>;
  public user: Observable<User>;

  baseUrl = environment.baseUrl + 'api/';
  empBaseUrl = this.baseUrl + 'employees';
  socialLoginUrl = environment.baseUrl + 'oauth/token';

  constructor(private http: HttpClient, private router: Router, private commonService: CommonService) {
    let userData = commonService.getItem('user');
    this.userSubject = new BehaviorSubject<User>(JSON.parse(userData));
    this.user = this.userSubject.asObservable();
  }

  public get userValue() {
    return this.userSubject.value;
  }

  /* authenticate the user */
  login(email, password) {
    return this.http.post<any>(`${this.baseUrl}login`, { email, password })
      .pipe(map(response => {
        if (response && response.access_token) {
          this.commonService.setItem('user', response);
          this.userSubject.next(response);
        }
        return response;
      }));
  }

  /* social login with google account */
  socialLogin(provider, socialUser) {
    let formData: FormData = new FormData();
    formData.append('grant_type', environment.grantType);
    formData.append('client_id', environment.clientId);
    formData.append('client_secret', environment.clientSecret);
    formData.append('provider', provider);
    formData.append('access_token', socialUser.response.access_token);

    return this.http.post<any>(`${this.socialLoginUrl}`, formData)
      .pipe(map(response => {
        response['data'] = { 'name': socialUser.name, 'email': socialUser.email };
        if (response && response.access_token) {
          this.commonService.setItem('user', response);
          this.userSubject.next(response);
        }
        return response;
      }));
  }

  /* logout the user */
  logout() {
    this.commonService.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/auth/login']);
  }

  /* register the user */
  register(user: User) {
    return this.http.post(`${this.baseUrl}register`, user);
  }

  /* fetch the employee list */
  getEmployeesList(): Observable<any> {
    return this.http.get(`${this.empBaseUrl}`);
  }

  /* add/create the employee */
  createEmployee(employee: Object): Observable<Object> {
    let formData: any = new FormData();
    formData.append("firstname", employee['firstname']);
    formData.append("lastname", employee['lastname']);
    formData.append("email", employee['email']);
    formData.append("image", employee['imageSource']);
    formData.append("age", employee['age']);
    formData.append("salary", employee['salary']);

    return this.http.post(`${this.empBaseUrl}`, formData);
  }

  /* fetch the employee by id */
  getEmployee(id: number): Observable<any> {
    return this.http.get(`${this.empBaseUrl}/${id}`);
  }

  /* update the employee */
  updateEmployee(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.empBaseUrl}/${id}`, value);
  }

  /* delete the employee */
  deleteEmployee(id: number): Observable<any> {
    return this.http.delete(`${this.empBaseUrl}/${id}`, { responseType: 'text' });
  }
}
