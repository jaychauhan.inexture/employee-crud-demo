import { Component } from '@angular/core';
import { User } from './_models/user';
import { EmployeeService } from './_services/employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular 11 - Employee CRUD Demo';
  user: User;

  constructor(private empService: EmployeeService) {
    this.empService.user.subscribe(x => this.user = x);
  }

  logout() {
    this.empService.logout();
  }
}
